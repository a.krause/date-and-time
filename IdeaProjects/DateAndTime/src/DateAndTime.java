import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;

public class DateAndTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        Map<String, String> zoneIds = new HashMap<>();
        zoneIds.put("Moscow", "Europe/Moscow");
        zoneIds.put("Oslo", "Europe/Oslo");
        zoneIds.put("Dublin", "Europe/Dublin");
        zoneIds.put("Budapest", "Europe/Budapest");
        zoneIds.put("Sydney", "Australia/Sydney");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("'Data:' dd-MM-YYYY'\nTime:'HH:mm:ss'\nZone:'0000','zzzz");
        System.out.println("Choise city,you can choose : Moscow,Oslo,Dublin,Budapest,Sydney");
        input = scanner.nextLine();
        if (input.equalsIgnoreCase("Moscow")) {
            String TimeZone = zoneIds.get("Moscow");
            ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(TimeZone));
            System.out.println(zonedDateTime.format(dateTimeFormatter));

        } else if (input.equalsIgnoreCase("Oslo")) {
            String TimeZone = zoneIds.get("Oslo");
            ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(TimeZone));
            System.out.println(zonedDateTime.format(dateTimeFormatter));
        }if (input.equalsIgnoreCase("Dublin")) {
            String TimeZone = zoneIds.get("Dublin");
            ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of(TimeZone));
            System.out.println(zonedDateTime.format(dateTimeFormatter));
        }if (input.equalsIgnoreCase("Budapest")){
            String TimeZone=zoneIds.get("Budapest");
            ZonedDateTime zonedDateTime=ZonedDateTime.now(ZoneId.of(TimeZone));
            System.out.println(zonedDateTime.format(dateTimeFormatter));
        }if (input.equalsIgnoreCase("Sydney")){
            String TimeZone=zoneIds.get("Sydney");
            ZonedDateTime zonedDateTime=ZonedDateTime.now(ZoneId.of(TimeZone));
            System.out.println(zonedDateTime.format(dateTimeFormatter));
        }
        if (!input.equalsIgnoreCase("quit"));

    }
}
// a.krause